package br.unipac.npj.repository;
import br.unipac.npj.model.*;
import java.util.List;



import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
@Transactional(readOnly = true)
public interface VisitasRepository extends JpaRepository<Visitas, Long> {

	List<Visitas> findByIdaluno(int idaluno);
	@Modifying
	@Transactional
	@Query("delete from Visitas u where u.idvisita = ?1")
	void deleteByIdvisita(int idvisita);
	
}
