package br.unipac.npj.repository;

import java.util.List;
import br.unipac.npj.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface AlunosRepository extends CrudRepository<Alunos, Long> {
	
	@Query("select u from Alunos u where u.nome like %:nome%")
	List<Alunos> findByNomeLike(@Param("nome") String nome);
	Alunos findById(int id);
	Alunos findByCpf(String cpf);
	Page<Alunos> findAll(Pageable pageable);
	


}
