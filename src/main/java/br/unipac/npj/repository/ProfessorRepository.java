package br.unipac.npj.repository;

import org.springframework.data.repository.CrudRepository;

import br.unipac.npj.model.Professor;

public interface ProfessorRepository extends CrudRepository<Professor, Long> { 


	Professor findById(int id);
	
}
