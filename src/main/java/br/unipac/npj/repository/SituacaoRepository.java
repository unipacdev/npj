package br.unipac.npj.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.unipac.npj.model.NotasForense;
import br.unipac.npj.model.Situacao;

public interface SituacaoRepository extends CrudRepository<Situacao, Long> {
	
	Situacao findById(int id);

	
}
