package br.unipac.npj.repository;

import br.unipac.npj.model.*;
import org.springframework.data.repository.CrudRepository;

public interface NotasForenseRepository extends CrudRepository<NotasForense, Long> {

	NotasForense findByIdaluno(int idaluno);
}
