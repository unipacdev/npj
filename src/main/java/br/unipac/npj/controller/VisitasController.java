package br.unipac.npj.controller;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.unipac.npj.model.Visitas;
import br.unipac.npj.repository.VisitasRepository;

@Controller
public class VisitasController {

	private VisitasRepository vr;
	
	@Autowired
	public VisitasController(VisitasRepository vr) {
		// TODO Auto-generated constructor stub
		this.vr = vr;
	}
	
	@RequestMapping(value = "/npj/delete/visitas/{idvisita}/{idaluno}/", method=RequestMethod.GET)
	public String delete(@PathVariable("idvisita") Integer idvisita,@PathVariable("idaluno") Integer idaluno,
			RedirectAttributes redirectAttributes) {
		
		vr.deleteByIdvisita(idvisita);

		redirectAttributes.addFlashAttribute("visitasdeleted", "O conteúdo selecionado foi deletado com sucesso!");
		
		return "redirect:/npj/edit/notas/" + idaluno;
	}
	
	
	@RequestMapping(value = "/npj/create/visitas/", method=RequestMethod.POST)
	public String create(@Valid @ModelAttribute("visitasform") Visitas visitasform, 
			Errors errors, RedirectAttributes redirectAttributes) {
		
		Visitas v = new Visitas(visitasform.getIdaluno(), visitasform.getData(), visitasform.getHistorico());
		
		if(vr.save(v) != null) {
			redirectAttributes.addFlashAttribute("visitassuccess", "Um novo conteúdo foi adicionado com sucesso! ");
		}
		
		return "redirect:/npj/edit/notas/" + visitasform.getIdaluno();
		
		
	}
}
