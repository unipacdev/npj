package br.unipac.npj.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.unipac.npj.model.Alunos;
import br.unipac.npj.model.Visitas;
import br.unipac.npj.model.NotasForense;
import br.unipac.npj.model.Professor;
import br.unipac.npj.model.Situacao;
import br.unipac.npj.repository.AlunosRepository;
import br.unipac.npj.repository.NotasForenseRepository;
import br.unipac.npj.repository.ProfessorRepository;
import br.unipac.npj.repository.SituacaoRepository;
import br.unipac.npj.repository.VisitasRepository;

@Controller
public class AlunoController {
	
	private AlunosRepository ar;
	private NotasForenseRepository nfr;
	private VisitasRepository vr;
	private ProfessorRepository pr;
	private SituacaoRepository sr;

	
	@Autowired
	public AlunoController(AlunosRepository ar, NotasForenseRepository nfr,
			ProfessorRepository pr, VisitasRepository vr, SituacaoRepository sr) {
		this.ar = ar;
		this.nfr = nfr;
		this.vr = vr;
		this.pr = pr;
		this.sr = sr;
		
	}

	
	@RequestMapping(value = "/npj/create", method = RequestMethod.POST)
	public String create(@Valid @ModelAttribute("alunos") Alunos alunos,Errors errors, 
			RedirectAttributes redirectAttributes) {


		Alunos a = new Alunos(alunos.getEstado_civil(), alunos.getRg(), alunos.getCpf(), alunos.getTel(), 
				alunos.getCel(), alunos.getNome(), alunos.getPai(), alunos.getMae(), alunos.getNaturalidade(),
				alunos.getEnd_rua(), alunos.getEnd_numero(), alunos.getEnd_bairro(), alunos.getEnd_cidade(), alunos.getEnd_cep(),
				alunos.getEnd_uf(), null, null );
		ar.save(a);
		
		NotasForense nf = new NotasForense(a.getId(),9,9,9,9,0,0,0,0,2,2,2,2,
				"   ", "   ", "   ", "   ", "   ","   ","   ", "   ",0,0,0,0,0,0,0,0);
		nfr.save(nf);
		redirectAttributes.addFlashAttribute("success", "O aluno foi cadastrado com sucesso com ID: " + a.getId());
		
		return "redirect:/npj/registrar-aluno";

	}
	
	@RequestMapping(value = "/npj/page/{pageNumber}", method = RequestMethod.GET)
	public String listAlunos(@PathVariable Integer pageNumber,Model model) {
		Page<Alunos> users = ar.findAll(new PageRequest(pageNumber, 20));


		
		int current = users.getNumber() + 1;
		int end = Math.max(1, current - 5);
		int begin = Math.min(end + 10, users.getTotalPages());
		String baseUrl = "/npj/page/";
		model.addAttribute("allist",users);
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);
		model.addAttribute("baseUrl", baseUrl);
		
		
		return "npj/index";
	}
	
	
	
	@RequestMapping(value = "/npj/edit/notas/{idaluno}", method = RequestMethod.GET)
	public String editnotas(@PathVariable Integer idaluno, NotasForense nform,Visitas visitasform,Model model) {
		
		
		
		Alunos aluno = ar.findById(idaluno);
		NotasForense nf = nfr.findByIdaluno(idaluno);
		List<Visitas> visitas = vr.findByIdaluno(idaluno);

		
		
		
		model.addAttribute("nome", aluno.getNome());
		model.addAttribute("id_aluno",nf.getIdaluno());	
		model.addAttribute("nform", new NotasForense());
		model.addAttribute("visitas",visitas);
		model.addAttribute("situacaolist", sr.findAll());
		model.addAttribute("professorlist", pr.findAll());
		model.addAttribute("visitasform", new Visitas());

		
		model.addAttribute("soma_frequencia", nf.getSete_frequencia() + 
				nf.getOito_frequencia() + nf.getNove_frequencia() + nf.getDez_frequencia());
		
		/* Sétimo Período */
		model.addAttribute("sete_ano",nf.getSete_ano());
		model.addAttribute("semestre7",nf.getSemestre7());
		model.addAttribute("sete_area",nf.getSete_area());
		model.addAttribute("sete_prof",pr.findById(nf.getSete_id_professor()).getNome());
		model.addAttribute("sete_prof_id",nf.getSete_id_professor());
		model.addAttribute("sete_situacao",sr.findById(nf.getSete_situacao()).getDescricao());
		model.addAttribute("sete_situacao_id",nf.getSete_situacao());
		model.addAttribute("sete_nota_1",nf.getSete_nota1());
		model.addAttribute("sete_nota_2",nf.getSete_nota2());
		model.addAttribute("sete_nota_3",nf.getSete_nota3());
		model.addAttribute("sete_frequencia",nf.getSete_frequencia());
		model.addAttribute("sete_nota_final",nf.getSete_nota1() + nf.getSete_nota2() + nf.getSete_nota3());
		model.addAttribute("sete_obs",nf.getSete_obs());
		model.addAttribute("sete_apto",nf.getSete_apto());
		
		/* Oitavo Período */
		model.addAttribute("oito_ano",nf.getOito_ano());
		model.addAttribute("semestre8",nf.getSemestre8());
		model.addAttribute("oito_area",nf.getOito_area());
		model.addAttribute("oito_prof",pr.findById(nf.getOito_id_professor()).getNome());
		model.addAttribute("oito_prof_id",nf.getOito_id_professor());
		model.addAttribute("oito_situacao",sr.findById(nf.getOito_situacao()).getDescricao());
		model.addAttribute("oito_situacao_id",nf.getOito_situacao());
		model.addAttribute("oito_nota_1",nf.getOito_nota1());
		model.addAttribute("oito_nota_2",nf.getOito_nota2());
		model.addAttribute("oito_nota_3",nf.getOito_nota3());
		model.addAttribute("oito_frequencia",nf.getOito_frequencia());
		model.addAttribute("oito_nota_final",nf.getOito_nota1() + nf.getOito_nota2() + nf.getOito_nota3());
		model.addAttribute("oito_obs",nf.getOito_obs());
		model.addAttribute("oito_apto",nf.getOito_apto());
		
		/* Nono Período */
		model.addAttribute("nove_ano",nf.getNove_ano());
		model.addAttribute("semestre9",nf.getSemestre9());
		model.addAttribute("nove_area",nf.getNove_area());
		model.addAttribute("nove_prof",pr.findById(nf.getNove_id_professor()).getNome());
		model.addAttribute("nove_prof_id",nf.getNove_id_professor());
		model.addAttribute("nove_situacao",sr.findById(nf.getNove_situacao()).getDescricao());
		model.addAttribute("nove_situacao_id",nf.getNove_situacao());
		model.addAttribute("nove_nota_1",nf.getNove_nota1());
		model.addAttribute("nove_nota_2",nf.getNove_nota2());
		model.addAttribute("nove_nota_3",nf.getNove_nota3());
		model.addAttribute("nove_frequencia",nf.getNove_frequencia());
		model.addAttribute("nove_nota_final",nf.getNove_nota1() + nf.getNove_nota2() + nf.getNove_nota3());
		model.addAttribute("nove_obs",nf.getNove_obs());
		model.addAttribute("nove_apto",nf.getNove_apto());
		
		/* Décimo Período */
		model.addAttribute("dez_ano",nf.getDez_ano());
		model.addAttribute("semestre10",nf.getSemestre10());
		model.addAttribute("dez_area",nf.getDez_area());
		model.addAttribute("dez_prof",pr.findById(nf.getDez_id_professor()).getNome());
		model.addAttribute("dez_prof_id",nf.getDez_id_professor());
		model.addAttribute("dez_situacao",sr.findById(nf.getDez_situacao()).getDescricao());
		model.addAttribute("dez_situacao_id",nf.getDez_situacao());
		model.addAttribute("dez_nota_1",nf.getDez_nota1());
		model.addAttribute("dez_nota_2",nf.getDez_nota2());
		model.addAttribute("dez_nota_3",nf.getDez_nota3());
		model.addAttribute("dez_frequencia",nf.getDez_frequencia());
		model.addAttribute("dez_nota_final",nf.getDez_nota1() + nf.getDez_nota2() + nf.getDez_nota3());
		model.addAttribute("dez_obs",nf.getDez_obs());
		model.addAttribute("dez_apto",nf.getDez_apto());
		

		
		
		
		
		
		return "npj/notas_forense";
	}
	
	
	@RequestMapping(value = "/npj/edit/notas/decimo/periodo", method = RequestMethod.POST)
	public String editDecidoPeriodo(@Valid @ModelAttribute("nform") NotasForense nform, 
			Errors errors, RedirectAttributes redirectAttributes) {
		
		NotasForense nf = nfr.findByIdaluno(nform.getIdaluno());
		nf.setDez_ano(nform.getDez_ano());
		nf.setSemestre10(nform.getSemestre10());
		nf.setDez_area(nform.getDez_area());
		nf.setDez_id_professor(nform.getDez_id_professor());
		nf.setDez_situacao(nform.getDez_situacao());
		nf.setDez_nota1(nform.getDez_nota1());
		nf.setDez_nota2(nform.getDez_nota2());
		nf.setDez_nota3(nform.getDez_nota3());
		nf.setDez_frequencia(nform.getDez_frequencia());
		nf.setDez_obs(nform.getDez_obs());
		nf.setDez_apto(nform.getDez_apto());	
		if(nfr.save(nf) != null) {
			
			redirectAttributes.addFlashAttribute("success", "Os dados do décimo período foram atualizados com sucesso!");
		}
		
		return "redirect:/npj/edit/notas/" + nform.getIdaluno();
	}
	
	@RequestMapping(value = "/npj/edit/notas/nono/periodo", method = RequestMethod.POST)
	public String editNonoPeriodo(@Valid @ModelAttribute("nform") NotasForense nform, 
			Errors errors, RedirectAttributes redirectAttributes) {
		
		NotasForense nf = nfr.findByIdaluno(nform.getIdaluno());
		nf.setNove_ano(nform.getNove_ano());
		nf.setSemestre9(nform.getSemestre9());
		nf.setNove_area(nform.getNove_area());
		nf.setNove_id_professor(nform.getNove_id_professor());
		nf.setNove_situacao(nform.getNove_situacao());
		nf.setNove_nota1(nform.getNove_nota1());
		nf.setNove_nota2(nform.getNove_nota2());
		nf.setNove_nota3(nform.getNove_nota3());
		nf.setNove_frequencia(nform.getNove_frequencia());
		nf.setNove_obs(nform.getNove_obs());
		nf.setNove_apto(nform.getNove_apto());	
		if(nfr.save(nf) != null) {
			
			redirectAttributes.addFlashAttribute("success", "Os dados do nono período foram atualizados com sucesso!");
		}
		
		return "redirect:/npj/edit/notas/" + nform.getIdaluno();
	}
	
	
	
	
	@RequestMapping(value = "/npj/edit/notas/oitavo/periodo", method = RequestMethod.POST)
	public String editOitavoPeriodo(@Valid @ModelAttribute("nform") NotasForense nform, 
			Errors errors, RedirectAttributes redirectAttributes) {
		
		NotasForense nf = nfr.findByIdaluno(nform.getIdaluno());
		nf.setOito_ano(nform.getOito_ano());
		nf.setSemestre8(nform.getSemestre8());
		nf.setOito_area(nform.getOito_area());
		nf.setOito_id_professor(nform.getOito_id_professor());
		nf.setOito_situacao(nform.getOito_situacao());
		nf.setOito_nota1(nform.getOito_nota1());
		nf.setOito_nota2(nform.getOito_nota2());
		nf.setOito_nota3(nform.getOito_nota3());
		nf.setOito_frequencia(nform.getOito_frequencia());
		nf.setOito_obs(nform.getOito_obs());
		nf.setOito_apto(nform.getOito_apto());	
		if(nfr.save(nf) != null) {
			
			redirectAttributes.addFlashAttribute("success", "Os dados do oitavo período foram atualizados com sucesso!");
		}
		
		return "redirect:/npj/edit/notas/" + nform.getIdaluno();
	}
	
	
	
	
	
	@RequestMapping(value = "/npj/edit/notas/setimo/periodo", method = RequestMethod.POST)
	public String editSetimoPeriodo(@Valid @ModelAttribute("nform") NotasForense nform, 
			Errors errors, RedirectAttributes redirectAttributes) {

		NotasForense nf = nfr.findByIdaluno(nform.getIdaluno());
		nf.setSete_ano(nform.getSete_ano());
		nf.setSemestre7(nform.getSemestre7());
		nf.setSete_area(nform.getSete_area());
		nf.setSete_id_professor(nform.getSete_id_professor());
		nf.setSete_situacao(nform.getSete_situacao());
		nf.setSete_nota1(nform.getSete_nota1());
		nf.setSete_nota2(nform.getSete_nota2());
		nf.setSete_nota3(nform.getSete_nota3());
		nf.setSete_frequencia(nform.getSete_frequencia());
		nf.setSete_obs(nform.getSete_obs());
		nf.setSete_apto(nform.getSete_apto());	
		if(nfr.save(nf) != null) {
			
			redirectAttributes.addFlashAttribute("success", "Os dados do sétimo período foram atualizados com sucesso!");
		}
		
		return "redirect:/npj/edit/notas/" + nform.getIdaluno();
	}
	
	
	
	@RequestMapping(value = "/npj/aluno/search", method = RequestMethod.GET)
	public String search(@RequestParam("q") String q,Model model) {

		List<Alunos> aluno = ar.findByNomeLike(q);
		
		model.addAttribute("allist",aluno);
		model.addAttribute("beginIndex", 0);
		model.addAttribute("endIndex", 0);
		model.addAttribute("currentIndex", 0);
		model.addAttribute("baseUrl", "dsds");
		
		return "npj/index";
	}
	
	
	@RequestMapping(value = "/npj/edit/alunos/{idaluno}", method=RequestMethod.GET)
	public String editAlunos(@PathVariable Integer idaluno, @Valid @ModelAttribute("nform") Alunos nform, Model model) {
		
	
		Alunos aluno = ar.findById(idaluno);
		
		model.addAttribute("id", aluno.getId());	
		model.addAttribute("nome", aluno.getNome());	
		model.addAttribute("pai", aluno.getPai());
		model.addAttribute("mae", aluno.getMae());
		model.addAttribute("naturalidade", aluno.getNaturalidade());
		model.addAttribute("estado_civil", aluno.getEstado_civil());
		model.addAttribute("data_nascimetno", aluno.getData_nascimento());
		model.addAttribute("rg", aluno.getRg());
		model.addAttribute("cpf", aluno.getCpf());
		model.addAttribute("telefone", aluno.getTel());
		model.addAttribute("celular", aluno.getCel());
		model.addAttribute("rua", aluno.getEnd_rua());
		model.addAttribute("numero", aluno.getEnd_numero());
		model.addAttribute("bairro", aluno.getEnd_bairro());
		model.addAttribute("cidade", aluno.getEnd_cidade());
		model.addAttribute("cep", aluno.getEnd_cep());
		model.addAttribute("uf", aluno.getEnd_uf());
		
		return "npj/editar_aluno";
	}
	
	@RequestMapping(value = "/npj/edit/alunos/update", method = RequestMethod.POST)
	public String atualizarAluno(@Valid @ModelAttribute("nform") Alunos nform, 
			Errors errors, RedirectAttributes redirectAttributes) {
		
		Alunos aluno = ar.findById(nform.getId());
		System.out.println("TEL " +  nform.getTel());
		System.out.println("CEL " +  nform.getCel());
		aluno.setNome(nform.getNome());
		aluno.setPai(nform.getPai());
		aluno.setMae(nform.getMae());
		aluno.setNaturalidade(nform.getNaturalidade());
		aluno.setEstado_civil(nform.getEstado_civil());
		aluno.setRg(nform.getRg());
		aluno.setCpf(nform.getCpf());
		aluno.setTel(nform.getTel());
		aluno.setCel(nform.getCel());
		aluno.setEnd_rua(nform.getEnd_rua());
		aluno.setEnd_numero(nform.getEnd_numero());
		aluno.setEnd_bairro(nform.getEnd_bairro());
		aluno.setEnd_cidade(nform.getEnd_cidade());
		aluno.setEnd_cep(nform.getEnd_cep());
		aluno.setEnd_uf(nform.getEnd_uf());
		
		
		if(ar.save(aluno) != null) {
			
			redirectAttributes.addFlashAttribute("success", "Os dados do aluno foram atualizados com sucesso!");
		}
		
		return "redirect:/npj/edit/alunos/" + nform.getId();
/*		System.out.println("ID ALUNO" + nform.getId());
		System.out.println("NOME " + nform.getNome());
		System.out.println("PAI "  + nform.getPai());
		System.out.println("Mae"  + nform.getMae());
		System.out.println("Naturalidade " + nform.getNaturalidade());
		System.out.println("Estado Civil " + nform.getEstado_civil());
		System.out.println("Data Nascimento " + nform.getData_nascimento());
		System.out.println("RG " + nform.getRg());
		System.out.println("CPF " + nform.getCpf());
		System.out.println("Telefone " + nform.getTel());
		System.out.println("Celular " + nform.getCel());
		System.out.println("RUA " + nform.getEnd_rua());
		System.out.println("NUMERO " + nform.getEnd_numero());
		System.out.println("BAIRRO " + nform.getEnd_bairro());
		System.out.println("CIDADE " + nform.getEnd_cidade());
		System.out.println("CEP " + nform.getEnd_cep());
		System.out.println("UF" + nform.getEnd_uf());*/
		
		
		

		
	}
	


	
	

	

	
}
