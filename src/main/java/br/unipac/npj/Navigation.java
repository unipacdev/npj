package br.unipac.npj;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.unipac.npj.model.Alunos;
import br.unipac.npj.repository.*;

@Controller
public class Navigation {
	@Autowired
	private AlunosRepository ar;
	
	@RequestMapping("/npj")
	public String dashboard() {
		return "redirect:/npj/page/1";
	}
	
	@RequestMapping("/npj/registrar-aluno")
	public String createAluno(Alunos alunos) {
		return "npj/cadastrar-aluno";
	}
	
	@RequestMapping(value = "/npj/edit/notas/{idaluno}")
	public String notas_forense(@PathVariable Integer idaluno) {
		return "redirect:/npj/edit/notas/" + idaluno;
	}
	
	

}
