package br.unipac.npj.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "npj_visitas")
public class Visitas {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_visita")
	private int idvisita;
	@Column(name="id_aluno")
	private int idaluno;
	private String data;
	private String historico;
	
	public Visitas(){}
	
	
	public Visitas(int idaluno, String data, String historico) {
		super();
		this.idaluno = idaluno;
		this.data = data;
		this.historico = historico;
	}
	
	public int getId_visita() {
		return idvisita;
	}
	public void setId_visita(int idvisita) {
		this.idvisita = idvisita;
	}
	public int getIdaluno() {
		return idaluno;
	}
	public void setIdaluno(int idaluno) {
		this.idaluno = idaluno;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getHistorico() {
		return historico;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
