package br.unipac.npj.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Entity
@Table(name = "npj_notas_forense")
public class NotasForense {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_nota_forense")
	private Integer id;
	@Column(name="id_aluno")
	private Integer idaluno;
	
	/* Notas Sétimo Período */

	private float sete_nota1;
	private float sete_nota2;
	private float sete_nota3;
	private float sete_frequencia;
	
	/* Notas Oitavo Período */

	private float oito_nota1;
	private float oito_nota2;
	private float oito_nota3;
	private float oito_frequencia;
	
	/* Notas Novo Período */

	private float nove_nota1;
	private float nove_nota2;
	private float nove_nota3;
	private float nove_frequencia;
	
	/* Notas Décimo Período */

	private float dez_nota1;
	private float dez_nota2;
	private float dez_nota3;

	private float dez_frequencia;
	

	private Integer sete_id_professor;

	
	private Integer semestre7;

	private Integer semestre10;

	private Integer semestre9;

	private Integer semestre8;
	

	private Integer sete_ano;

	private Integer oito_ano;

	private Integer nove_ano;

	private Integer dez_ano;
	

	private Integer oito_id_professor;

	private Integer nove_id_professor;

	private Integer dez_id_professor;
	

	private String sete_obs;

	private String oito_obs;

	private String nove_obs;

	private String dez_obs;
	

	private Integer sete_situacao;

	private Integer oito_situacao;

	private Integer nove_situacao;

	private Integer dez_situacao;
	
	private Integer sete_apto;

	private Integer oito_apto;

	private Integer nove_apto;

	private Integer dez_apto;
	
	private String sete_area;
	private String oito_area;
	private String nove_area;
	private String dez_area;
	
	
	public NotasForense() {}











	public NotasForense(Integer idaluno, Integer sete_id_professor, Integer oito_id_professor,
			Integer nove_id_professor, Integer dez_id_professor, Integer sete_situacao, Integer oito_situacao,
			Integer nove_situacao, Integer dez_situacao, Integer sete_apto, Integer oito_apto, Integer nove_apto,
			Integer dez_apto,String sete_obs,String oito_obs,String nove_obs,String dez_obs,
			String sete_area,String oito_area,String nove_area,String dez_area,Integer sete_ano,Integer oito_ano,Integer nove_ano,
			Integer dez_ano,Integer semestre7,Integer semestre8,Integer semestre9,Integer semestre10) {
		super();
		this.idaluno = idaluno;
		this.sete_id_professor = sete_id_professor;
		this.oito_id_professor = oito_id_professor;
		this.nove_id_professor = nove_id_professor;
		this.dez_id_professor = dez_id_professor;
		this.sete_situacao = sete_situacao;
		this.oito_situacao = oito_situacao;
		this.nove_situacao = nove_situacao;
		this.dez_situacao = dez_situacao;
		this.sete_apto = sete_apto;
		this.oito_apto = oito_apto;
		this.nove_apto = nove_apto;
		this.dez_apto = dez_apto;
		this.sete_obs = sete_obs;
		this.oito_obs = oito_obs;
		this.nove_obs = nove_obs;
		this.dez_obs = dez_obs;
		this.sete_area = sete_area;
		this.oito_area = oito_area;
		this.nove_area = nove_area;
		this.dez_area = dez_area;
		this.sete_ano = sete_ano;
		this.oito_ano = oito_ano;
		this.nove_ano = nove_ano;
		this.dez_ano = dez_ano;
		this.semestre7 = semestre7;
		this.semestre8 = semestre8;
		this.semestre9 = semestre9;
		this.semestre10 = semestre10;
		
	}











	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdaluno() {
		return idaluno;
	}
	public void setIdaluno(Integer idaluno) {
		this.idaluno = idaluno;
	}
	public float getSete_nota1() {
		return sete_nota1;
	}
	public void setSete_nota1(float sete_nota1) {
		this.sete_nota1 = sete_nota1;
	}
	public float getSete_nota2() {
		return sete_nota2;
	}
	public void setSete_nota2(float sete_nota2) {
		this.sete_nota2 = sete_nota2;
	}
	public float getSete_nota3() {
		return sete_nota3;
	}
	public void setSete_nota3(float sete_nota3) {
		this.sete_nota3 = sete_nota3;
	}
	public float getSete_frequencia() {
		return sete_frequencia;
	}
	public void setSete_frequencia(float sete_frequencia) {
		this.sete_frequencia = sete_frequencia;
	}
	public float getOito_nota1() {
		return oito_nota1;
	}
	public void setOito_nota1(float oito_nota1) {
		this.oito_nota1 = oito_nota1;
	}
	public float getOito_nota2() {
		return oito_nota2;
	}
	public void setOito_nota2(float oito_nota2) {
		this.oito_nota2 = oito_nota2;
	}
	public float getOito_nota3() {
		return oito_nota3;
	}
	public void setOito_nota3(float oito_nota3) {
		this.oito_nota3 = oito_nota3;
	}
	public float getOito_frequencia() {
		return oito_frequencia;
	}
	public void setOito_frequencia(float oito_frequencia) {
		this.oito_frequencia = oito_frequencia;
	}
	public float getNove_nota1() {
		return nove_nota1;
	}
	public void setNove_nota1(float nove_nota1) {
		this.nove_nota1 = nove_nota1;
	}
	public float getNove_nota2() {
		return nove_nota2;
	}
	public void setNove_nota2(float nove_nota2) {
		this.nove_nota2 = nove_nota2;
	}
	public float getNove_nota3() {
		return nove_nota3;
	}
	public void setNove_nota3(float nove_nota3) {
		this.nove_nota3 = nove_nota3;
	}
	public float getNove_frequencia() {
		return nove_frequencia;
	}
	public void setNove_frequencia(float nove_frequencia) {
		this.nove_frequencia = nove_frequencia;
	}
	public float getDez_nota1() {
		return dez_nota1;
	}
	public void setDez_nota1(float dez_nota1) {
		this.dez_nota1 = dez_nota1;
	}
	public float getDez_nota2() {
		return dez_nota2;
	}
	public void setDez_nota2(float dez_nota2) {
		this.dez_nota2 = dez_nota2;
	}
	public float getDez_nota3() {
		return dez_nota3;
	}
	public void setDez_nota3(float dez_nota3) {
		this.dez_nota3 = dez_nota3;
	}
	public float getDez_frequencia() {
		return dez_frequencia;
	}
	public void setDez_frequencia(float dez_frequencia) {
		this.dez_frequencia = dez_frequencia;
	}
	public Integer getSete_id_professor() {
		return sete_id_professor;
	}
	public void setSete_id_professor(Integer sete_id_professor) {
		this.sete_id_professor = sete_id_professor;
	}
	public Integer getSemestre7() {
		return semestre7;
	}
	public void setSemestre7(Integer semestre7) {
		this.semestre7 = semestre7;
	}
	public Integer getSemestre10() {
		return semestre10;
	}
	public void setSemestre10(Integer semestre10) {
		this.semestre10 = semestre10;
	}
	public Integer getSemestre9() {
		return semestre9;
	}
	public void setSemestre9(Integer semestre9) {
		this.semestre9 = semestre9;
	}
	public Integer getSemestre8() {
		return semestre8;
	}
	public void setSemestre8(Integer semestre8) {
		this.semestre8 = semestre8;
	}
	public Integer getSete_ano() {
		return sete_ano;
	}
	public void setSete_ano(Integer sete_ano) {
		this.sete_ano = sete_ano;
	}
	public Integer getOito_ano() {
		return oito_ano;
	}
	public void setOito_ano(Integer oito_ano) {
		this.oito_ano = oito_ano;
	}
	public Integer getNove_ano() {
		return nove_ano;
	}
	public void setNove_ano(Integer nove_ano) {
		this.nove_ano = nove_ano;
	}
	public Integer getDez_ano() {
		return dez_ano;
	}
	public void setDez_ano(Integer dez_ano) {
		this.dez_ano = dez_ano;
	}
	public Integer getOito_id_professor() {
		return oito_id_professor;
	}
	public void setOito_id_professor(Integer oito_id_professor) {
		this.oito_id_professor = oito_id_professor;
	}
	public Integer getNove_id_professor() {
		return nove_id_professor;
	}
	public void setNove_id_professor(Integer nove_id_professor) {
		this.nove_id_professor = nove_id_professor;
	}
	public Integer getDez_id_professor() {
		return dez_id_professor;
	}
	public void setDez_id_professor(Integer dez_id_professor) {
		this.dez_id_professor = dez_id_professor;
	}
	public String getSete_obs() {
		return sete_obs;
	}
	public void setSete_obs(String sete_obs) {
		this.sete_obs = sete_obs;
	}
	public String getOito_obs() {
		return oito_obs;
	}
	public void setOito_obs(String oito_obs) {
		this.oito_obs = oito_obs;
	}
	public String getNove_obs() {
		return nove_obs;
	}
	public void setNove_obs(String nove_obs) {
		this.nove_obs = nove_obs;
	}
	public String getDez_obs() {
		return dez_obs;
	}
	public void setDez_obs(String dez_obs) {
		this.dez_obs = dez_obs;
	}
	public Integer getSete_situacao() {
		return sete_situacao;
	}
	public void setSete_situacao(Integer sete_situacao) {
		this.sete_situacao = sete_situacao;
	}
	public Integer getOito_situacao() {
		return oito_situacao;
	}
	public void setOito_situacao(Integer oito_situacao) {
		this.oito_situacao = oito_situacao;
	}
	public Integer getNove_situacao() {
		return nove_situacao;
	}
	public void setNove_situacao(Integer nove_situacao) {
		this.nove_situacao = nove_situacao;
	}
	public Integer getDez_situacao() {
		return dez_situacao;
	}
	public void setDez_situacao(Integer dez_situacao) {
		this.dez_situacao = dez_situacao;
	}
	public Integer getSete_apto() {
		return sete_apto;
	}
	public void setSete_apto(Integer sete_apto) {
		this.sete_apto = sete_apto;
	}
	public Integer getOito_apto() {
		return oito_apto;
	}
	public void setOito_apto(Integer oito_apto) {
		this.oito_apto = oito_apto;
	}
	public Integer getNove_apto() {
		return nove_apto;
	}
	public void setNove_apto(Integer nove_apto) {
		this.nove_apto = nove_apto;
	}
	public Integer getDez_apto() {
		return dez_apto;
	}
	public void setDez_apto(Integer dez_apto) {
		this.dez_apto = dez_apto;
	}
	public String getSete_area() {
		return sete_area;
	}
	public void setSete_area(String sete_area) {
		this.sete_area = sete_area;
	}
	public String getOito_area() {
		return oito_area;
	}
	public void setOito_area(String oito_area) {
		this.oito_area = oito_area;
	}
	public String getNove_area() {
		return nove_area;
	}
	public void setNove_area(String nove_area) {
		this.nove_area = nove_area;
	}
	public String getDez_area() {
		return dez_area;
	}
	public void setDez_area(String dez_area) {
		this.dez_area = dez_area;
	}
	

	
	
}
