package br.unipac.npj.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "npj_alunos")
public class Alunos {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_aluno")
	private int id;
	private String estado_civil;
	private String rg;
	private String cpf;
	private String tel;
	private String cel;
	private String nome;
	private String pai;
	private String mae;
	private String naturalidade;
	private String end_rua;
	private String end_numero;
	private String end_bairro;
	private String end_cidade;
	private String end_cep;
	private String end_uf;
	@Temporal(TemporalType.DATE)
	private Date data_nascimento;
	@Temporal(TemporalType.DATE) 
	private Date data_cadastro;
	
	
	protected Alunos() {
		
	}
	

	public Alunos(String estado_civil, String rg, String cpf, String tel, String cel, String nome,
			String pai, String mae, String naturalidade, String end_rua, String end_numero, String end_bairro,
			String end_cidade, String end_cep, String end_uf, Date data_nascimento, Date data_cadastro) {
		super();
		this.estado_civil = estado_civil;
		this.rg = rg;
		this.cpf = cpf;
		this.tel = tel;
		this.cel = cel;
		this.nome = nome;
		this.pai = pai;
		this.mae = mae;
		this.naturalidade = naturalidade;
		this.end_rua = end_rua;
		this.end_numero = end_numero;
		this.end_bairro = end_bairro;
		this.end_cidade = end_cidade;
		this.end_cep = end_cep;
		this.end_uf = end_uf;
		this.data_nascimento = data_nascimento;
		this.data_cadastro = data_cadastro;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getEstado_civil() {
		return estado_civil;
	}


	public void setEstado_civil(String estado_civil) {
		this.estado_civil = estado_civil;
	}


	public String getRg() {
		return rg;
	}


	public void setRg(String rg) {
		this.rg = rg;
	}


	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public String getTel() {
		return tel;
	}


	public void setTel(String tel) {
		this.tel = tel;
	}


	public String getCel() {
		return cel;
	}


	public void setCel(String cel) {
		this.cel = cel;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getPai() {
		return pai;
	}


	public void setPai(String pai) {
		this.pai = pai;
	}


	public String getMae() {
		return mae;
	}


	public void setMae(String mae) {
		this.mae = mae;
	}


	public String getNaturalidade() {
		return naturalidade;
	}


	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}


	public String getEnd_rua() {
		return end_rua;
	}


	public void setEnd_rua(String end_rua) {
		this.end_rua = end_rua;
	}


	public String getEnd_numero() {
		return end_numero;
	}


	public void setEnd_numero(String end_numero) {
		this.end_numero = end_numero;
	}


	public String getEnd_bairro() {
		return end_bairro;
	}


	public void setEnd_bairro(String end_bairro) {
		this.end_bairro = end_bairro;
	}


	public String getEnd_cidade() {
		return end_cidade;
	}


	public void setEnd_cidade(String end_cidade) {
		this.end_cidade = end_cidade;
	}


	public String getEnd_cep() {
		return end_cep;
	}


	public void setEnd_cep(String end_cep) {
		this.end_cep = end_cep;
	}


	public String getEnd_uf() {
		return end_uf;
	}


	public void setEnd_uf(String end_uf) {
		this.end_uf = end_uf;
	}


	public Date getData_nascimento() {
		return data_nascimento;
	}


	public void setData_nascimento(Date data_nascimento) {
		this.data_nascimento = data_nascimento;
	}


	public Date getData_cadastro() {
		return data_cadastro;
	}


	public void setData_cadastro(Date data_cadastro) {
		this.data_cadastro = data_cadastro;
	}
	

	
}
