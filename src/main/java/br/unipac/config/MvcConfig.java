package br.unipac.config;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

public class MvcConfig extends WebMvcConfigurerAdapter {
	
	@Bean(name = "dataSource")
	public DriverManagerDataSource dataSource() {
	    DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
	    driverManagerDataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	    driverManagerDataSource.setUrl("jdbc:sqlserver://187.85.82.116:1544;database=daj");
	    driverManagerDataSource.setUsername("sa");
	    driverManagerDataSource.setPassword("webmaster7534286$%983$#45");
	    return driverManagerDataSource;
	}

}
