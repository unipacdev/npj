package br.unipac.auth.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.unipac.auth.model.User;
import java.util.Optional;
public interface UserRepository extends CrudRepository<User, Integer> {
	
	Optional<User> findByUsername(String username);
	
}