package br.unipac.auth.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Auth {

	@RequestMapping("/")
	public String index() {
		return "redirect:/auth/login";
	}
	
	@RequestMapping("/auth/login")
	public String login() {
		return "auth/login";
	}
	
	@RequestMapping("/auth/register")
	public String create() {
		return "auth/register";
	}
	
	@RequestMapping("/auth/forgot-password") 
	public String reset() {
		return null;
	}
	
}

